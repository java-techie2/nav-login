import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.scss']
})
export class MyNavComponent implements OnInit {
  public isLoggedIn: Observable<boolean> = new Observable<boolean>();
  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
  }
  constructor(private authService: AuthService) {}
  public logout(): void {
    this.authService.logout();
  }

}
